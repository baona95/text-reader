import gi
import math
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Pango, Gdk

class ReaderWindow(Gtk.Window):
    PAGE_SIZE = 1024
    PADDING = 0
    PAGE_VIEW = True

    def __init__(self):
        Gtk.Window.__init__(self, title="Text Reader", default_height = 500, default_width = 700)

        self.grid = Gtk.Grid(column_homogeneous = True)
        self.add(self.grid)

        self.connect("check_resize", self.on_window_resized)

        openfile = Gtk.Button("Choose File")
        openfile.connect("clicked", self.on_file_clicked)
        self.grid.attach(openfile, 1, 1, 1, 1)

        prev_page = Gtk.Button(" < ")
        prev_page.connect("clicked", self.on_prev_page_clicked)
        self.grid.attach(prev_page, 2, 1, 1, 1)

        next_page = Gtk.Button(" > ")
        next_page.connect("clicked", self.on_next_page_clicked)
        self.grid.attach(next_page, 3, 1, 1, 1)

        self.connect("key_press_event", self.on_key_pressed)

        self.page_progress = Gtk.Label(label = "0 / 0 ", halign = Gtk.Align.END)
        self.grid.attach(self.page_progress, 4, 1, 1, 1)

        self.textview = Gtk.TextView(top_margin = self.PADDING, right_margin = self.PADDING, 
            bottom_margin = self.PADDING, left_margin = self.PADDING, 
            justification = Gtk.Justification.FILL)
        self.textview.set_editable(False)
        self.textview.set_cursor_visible(False)
        self.textview.set_wrap_mode(2)
        self.textview.set_hexpand(True)
        self.textview.set_vexpand(True)
        self.textbuffer = self.textview.get_buffer()

        self.scrolledwindow = Gtk.ScrolledWindow()
        self.scrolledwindow.add(self.textview)

        if self.PAGE_VIEW:
            self.scrolledwindow.set_policy(Gtk.PolicyType.EXTERNAL, Gtk.PolicyType.EXTERNAL)
            self.vadjustment = self.scrolledwindow.get_vadjustment()
            # self.vadjustment.connect("value_changed", self.on_vscroll_changed)

        self.grid.attach(self.scrolledwindow, 1, 2, 4, 1)

        self.font_tahoma = self.textbuffer.create_tag("font_tahoma", family="Tahoma")
        self.font_sansserif = self.textbuffer.create_tag("font_sansserif", family="Sans-serif")
        self.font_size = self.textbuffer.create_tag("font_size", size_points=17)
        self.background_color = self.textbuffer.create_tag("background_color", background="white")
        self.foreground_color = self.textbuffer.create_tag("foreground_color", foreground="black")
        self.line_height = self.textbuffer.create_tag("line_height", pixels_inside_wrap=10)

    def on_file_clicked(self, widget):
        dialog = Gtk.FileChooserDialog("Please choose a file", self,
            Gtk.FileChooserAction.OPEN,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        self.add_filters(dialog)

        print(self.get_widget_size(self.textview))
        self.textview_width, self.textview_height = self.get_widget_size(self.textview)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            print("Open clicked")
            print("File selected: " + dialog.get_filename())

            with open(dialog.get_filename(), encoding="utf-8") as file:
            	content = file.read()

            self.page_count = math.ceil(len(content) / ReaderWindow.PAGE_SIZE)

            self.textbuffer.set_text(content)

            self.set_page_progress(1, self.page_count)

            self.textbuffer.apply_tag(self.font_sansserif, 
            	self.textbuffer.get_start_iter(), self.textbuffer.get_end_iter())
            self.textbuffer.apply_tag(self.font_size, 
                self.textbuffer.get_start_iter(), self.textbuffer.get_end_iter())
            self.textbuffer.apply_tag(self.background_color, 
                self.textbuffer.get_start_iter(), self.textbuffer.get_end_iter())
            self.textbuffer.apply_tag(self.foreground_color, 
                self.textbuffer.get_start_iter(), self.textbuffer.get_end_iter())
            self.textbuffer.apply_tag(self.line_height, 
                self.textbuffer.get_start_iter(), self.textbuffer.get_end_iter())

        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()

    def on_vscroll_changed(self, adjustment):
        adjustment.set_value(0)
        return False

    def on_prev_page_clicked(self, widget):
        self.vadjustment.set_value(max(0, self.vadjustment.get_value() - self.textview_height))
        return False

    def on_next_page_clicked(self, widget):
        self.vadjustment.set_value(self.vadjustment.get_value() + self.textview_height)
        return False

    def on_key_pressed(self, widget, event):
        # print(Gdk.keyval_name(event.keyval))

        key_name = Gdk.keyval_name(event.keyval).lower()

        if key_name == 'a' or key_name == 'left':
            self.on_prev_page_clicked(widget)
        elif key_name == 'd' or key_name =='right':
            self.on_next_page_clicked(widget)

        return True

    def on_window_resized(self, widget):
        print(self.get_widget_size(self.textview))
        self.textview_width, self.textview_height = self.get_widget_size(self.textview)

    def add_filters(self, dialog):
        filter_text = Gtk.FileFilter()
        filter_text.set_name("Text files")
        filter_text.add_mime_type("text/plain")
        dialog.add_filter(filter_text)

        filter_py = Gtk.FileFilter()
        filter_py.set_name("Python files")
        filter_py.add_mime_type("text/x-python")
        dialog.add_filter(filter_py)

        filter_any = Gtk.FileFilter()
        filter_any.set_name("Any files")
        filter_any.add_pattern("*")
        dialog.add_filter(filter_any)

    def get_widget_size(self, widget: Gtk.Widget):
        return [widget.get_allocation().width, widget.get_allocation().height]

    def set_page_progress(self, current: int, max: int):
        self.page_progress.set_label("%s / %s " % (str(current), str(max)))


win = ReaderWindow()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()